import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  } 
  return (
    // <React.Fragment>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="home" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
      </div>
    </BrowserRouter>
    // </React.Fragment>
  );
}

export default App;


