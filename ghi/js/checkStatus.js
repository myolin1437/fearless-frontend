// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload')

if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(encodedPayload)

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload)

  // Print the payload
  console.log('PAYLOAD', payload);

  const permission = payload.user.perms;
  const navLinks = document.querySelectorAll('.nav-link');
  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (permission.includes('events.add_conference')) {
    navLinks[1].classList.remove('d-none');
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (permission.includes('events.add_location')) {
    navLinks[2].classList.remove('d-none');
  }

}